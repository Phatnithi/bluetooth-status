package com.example.bluetoothstatus

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.ACTION_ACL_CONNECTED
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val filter = IntentFilter()
        filter.addAction(ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        this.registerReceiver(broadcastReceiver, filter)
    }

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        var device: BluetoothDevice? = null
        override fun onReceive(context: Context, intent: Intent) {

            val icon = findViewById<ImageView>(R.id.bluetoothIcon)
            val txt = findViewById<TextView>(R.id.textView)
            val deviceName = findViewById<TextView>(R.id.textView2)
            val action = intent.action
            device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)

            if (ACTION_ACL_CONNECTED == action) {
                Toast.makeText(applicationContext, "Device is now Connected", Toast.LENGTH_SHORT).show()
                icon.visibility = View.VISIBLE
                txt.text = "Connected"
                deviceName.text = device!!.name

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED == action) {
                Toast.makeText(applicationContext, "Device is disconnected", Toast.LENGTH_SHORT).show()
                icon.visibility = View.INVISIBLE
                txt.text = "No Device"
                deviceName.text = "Device Disconnected"

            }
        }
    }
}


